/**
 * @description       :
 * @author            : ethan.k@digitalmass.com
 * @group             :
 * @last modified on  : 07-13-2020
 * @last modified by  : ethan.k@digitalmass.com
 * Modifications Log
 * Ver   Date         Author                    Modification
 * 1.0   07-13-2020   ethan.k@digitalmass.com   Initial Version
**/
public with sharing class ApexFirstClass {
    public ApexFirstClass() {
        String first_string = 'this is a string';
    }
}